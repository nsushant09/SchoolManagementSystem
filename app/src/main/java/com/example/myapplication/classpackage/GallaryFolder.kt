package com.example.myapplication.classpackage

class GallaryFolder constructor(private val year : Int, private val event : String , private val folderImages : ArrayList<String>) {

    fun getYear() = year
    fun getEvent() = event
    fun getFolderImages() = folderImages

}