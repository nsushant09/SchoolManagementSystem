package com.example.myapplication.classpackage

 class NotificationHome constructor(private val title : String, private val desc : String) {

    fun getTitle() : String = title
    fun getDesc() : String = desc
}