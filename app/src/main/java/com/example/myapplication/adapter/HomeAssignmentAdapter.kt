package com.example.myapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.classpackage.TodayAssignmentHome
import com.example.myapplication.databinding.HomeAssignmentRvCardBinding


class HomeAssignmentAdapter(private val context : Context, private val list : ArrayList<TodayAssignmentHome>)
    : RecyclerView.Adapter<HomeAssignmentAdapter.ViewHolder>()
{
        inner class ViewHolder(view : HomeAssignmentRvCardBinding) : RecyclerView.ViewHolder(view.root){
            val cardItem = view.homeAssignRvCard
            val subItem = view.homeAssignRvSubject
            val descItem = view.homeAssignRvDesc
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            HomeAssignmentRvCardBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.subItem.text = list.get(position).getSubject()
        holder.descItem.text = list.get(position).getAssignment()
    }

    override fun getItemCount(): Int = list.size
}