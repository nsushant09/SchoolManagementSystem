package com.example.myapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.classpackage.NotificationHome
import com.example.myapplication.R
import com.example.myapplication.databinding.HomeNotificationRvCardBinding

class HomeNotificationAdapter(private val context : Context, private val notificationList : ArrayList<NotificationHome>)
    : RecyclerView.Adapter<HomeNotificationAdapter.ViewHolder>()
{
        inner class ViewHolder(view : HomeNotificationRvCardBinding) : RecyclerView.ViewHolder(view.root){
            val cardItem  = view.homeNotiRvCard
            val titleItem = view.homeNotiRvTitle
            val descItem = view.homeNotiRvDesc
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            HomeNotificationRvCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleItem.text = notificationList.get(position).getTitle()
        holder.descItem.text = notificationList.get(position).getDesc()
    }

    override fun getItemCount(): Int {
        return notificationList.size
    }
}