package com.example.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.classpackage.GallaryFolder
import com.example.myapplication.databinding.GallaryFolderRvCardBinding
import com.example.myapplication.databinding.HomeNotificationRvCardBinding
import com.example.myapplication.studentfragments.StudentGallaryFolderImagesFragment
import com.example.myapplication.studentfragments.StudentHomeFragment
import com.squareup.picasso.Picasso
import kotlin.random.Random


class GallaryFolderAdapter(private val mcontext : Context, private val folderList : ArrayList<GallaryFolder>) : RecyclerView.Adapter<GallaryFolderAdapter.ViewHolder>(){

    inner class ViewHolder(view : GallaryFolderRvCardBinding) : RecyclerView.ViewHolder(view.root){
        val imageItem = view.gallaryFolderRvImage
        val yearItem = view.gallaryFolderRvYear
        val eventItem = view.gallaryFolderRvEvent
        val cardItem = view.cardGallaryFolder
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            GallaryFolderRvCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val firstImage = folderList.get(position).getFolderImages()[0]
        Picasso.get().load(firstImage).into(holder.imageItem)
        holder.yearItem.text = folderList.get(position).getYear().toString()
        holder.eventItem.text = folderList.get(position).getEvent()
        val folderImageList = folderList.get(position).getFolderImages()

        holder.cardItem.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val bundle = Bundle()
                bundle.putStringArrayList("folderImageList", folderImageList)
                val activity = v!!.context as AppCompatActivity
                val studentGallaryImagesFragment = StudentGallaryFolderImagesFragment()
                studentGallaryImagesFragment.arguments = bundle
                activity.supportFragmentManager.beginTransaction().replace(R.id.fragment_container,studentGallaryImagesFragment).commit()
            }
        })


    }

    override fun getItemCount(): Int {
        return folderList.size
    }
}