package com.example.myapplication.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.GallaryImageRvCardBinding
import com.squareup.picasso.Picasso

class GallaryFolderImagesAdapter(private val context: Context, private val imagesList : ArrayList<String>)
    : RecyclerView.Adapter<GallaryFolderImagesAdapter.ViewHolder>(){

        inner class ViewHolder(val binding : GallaryImageRvCardBinding) : RecyclerView.ViewHolder(binding.root){
            val imageItem = binding.imgPhoto
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            GallaryImageRvCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Picasso.get().load(imagesList.get(position)).into(holder.imageItem)
    }

    override fun getItemCount(): Int = imagesList.size
}