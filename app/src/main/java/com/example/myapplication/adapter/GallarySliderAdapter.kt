package com.example.myapplication.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.example.myapplication.classpackage.SliderImage
import com.example.myapplication.databinding.GallaryImageSliderItemBinding

class GallarySliderAdapter(val viewPager2: ViewPager2, val imgList : ArrayList<SliderImage>) : RecyclerView.Adapter<GallarySliderAdapter.Viewholder>() {

    inner class Viewholder(val binding : GallaryImageSliderItemBinding) : RecyclerView.ViewHolder(binding.root){
        val img = binding.gallaryImgSlider
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        return Viewholder(
            GallaryImageSliderItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        val listImg = imgList[position]
        holder.img.setImageResource(listImg.img)
        if(position == imgList.size - 2){
            viewPager2.post(run)
        }
    }

    override fun getItemCount(): Int {
        return imgList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    private val run = Runnable {
        imgList.addAll(imgList)
        notifyDataSetChanged()
    }

}