package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.myapplication.databinding.ActivityLoginBinding

class Login : AppCompatActivity() {
    private lateinit var binding : ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root )
        val btnLoginStudent : Button= findViewById(R.id.btnLoginStudent)
        val btnLoginAdmin : Button = findViewById(R.id.btnLoginAdmin)
        val gotoLoginasStudent = Intent(this,LoginAsStudent::class.java)
        val animation = supportFragmentManager.beginTransaction()
        btnLoginStudent.setOnClickListener{
            Intent(this, LoginAsStudent::class.java).also{
                startActivity(it)
                overridePendingTransition(androidx.appcompat.R.anim.abc_slide_in_bottom,androidx.appcompat.R.anim.abc_slide_out_top)
                finish()
            }
        }
    }
}