package com.example.myapplication.studentfragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.myapplication.R
import com.example.myapplication.adapter.GallaryFolderImagesAdapter
import com.example.myapplication.databinding.FragmentStudentGallaryFolderImagesBinding

class StudentGallaryFolderImagesFragment : Fragment() {

    private var _binding : FragmentStudentGallaryFolderImagesBinding ?= null
    private val binding get() = _binding!!
    private lateinit var imageList : ArrayList<String>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentStudentGallaryFolderImagesBinding.inflate(layoutInflater)
        val args = this.arguments
        if (args != null) {
            imageList = args.getStringArrayList("folderImageList") as ArrayList<String>
        }
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.gallaryFolderImagesRv.layoutManager = StaggeredGridLayoutManager(1, LinearLayoutManager.VERTICAL)
        val gallaryFolderImagesAdapter = GallaryFolderImagesAdapter(requireContext(), addImageList())
        binding.gallaryFolderImagesRv.adapter = gallaryFolderImagesAdapter

        binding.btnBackImage.setOnClickListener(object : View.OnClickListener{
            override fun onClick(v: View?) {
                val fragmentManager = v!!.context as AppCompatActivity
                val studentGallaryFragment = StudentGallaryFragment()
                fragmentManager.supportFragmentManager.beginTransaction().replace(R.id.fragment_container, studentGallaryFragment).commit()
            }
        })
    }

    fun addImageList() : ArrayList<String>{
        val args = this.arguments
        if (args != null) {
            imageList = args.getStringArrayList("folderImageList") as ArrayList<String>
        }

        return imageList
    }
}