package com.example.myapplication.studentfragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.MainActivity
import com.example.myapplication.R
import com.example.myapplication.adapter.MenuRecyclerAdapter
import com.example.myapplication.databinding.FragmentStudentMenuBinding
import com.example.myapplication.databinding.FragmentStudentSettingsBinding


class StudentMenuFragment : Fragment() {

    private var _binding : FragmentStudentMenuBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStudentMenuBinding.inflate(layoutInflater,container,false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.menuRv.layoutManager = LinearLayoutManager(context)
        val menuRecyclerAdapter  = MenuRecyclerAdapter(requireContext(),imageArray(),titleList(),descriptionList())
        binding.menuRv.adapter = menuRecyclerAdapter

    }

    fun imageArray() : ArrayList<Int>{
        val imageList = arrayListOf<Int>(

            R.drawable.google_classroom,
            R.drawable.exam,
            R.drawable.assignment,
            R.drawable.attendance,
            R.drawable.homework,
            R.drawable.report_card,
            R.drawable.books,
            R.drawable.event,
        )
        return imageList
    }

    fun titleList() : ArrayList<String>{
        val titleList = arrayListOf<String>(

            "Online Class",
            "E-Classroom",
            "Assignments",
            "Attendance",
            "Homework",
            "Report Card",
            "Reading Materials",
            "Events"

        )
        return titleList
    }

    fun descriptionList() : ArrayList<String>{
        val descriptionList = arrayListOf<String>(

            "View Active Online Classrooms",
            "Multiple Choice Questions",
            "View & Submit Assignment",
            "Monthly & Aggregate Report",
            "Daily Homework",
            "View Exam Results",
            "View Reading Material and Resources",
            "Program and Activities"

        )
        return descriptionList
    }
}