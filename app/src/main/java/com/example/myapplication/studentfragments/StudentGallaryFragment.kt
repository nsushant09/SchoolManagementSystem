package com.example.myapplication.studentfragments

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.widget.ViewPager2
import com.example.myapplication.R
import com.example.myapplication.adapter.GallaryFolderAdapter
import com.example.myapplication.adapter.GallarySliderAdapter
import com.example.myapplication.adapter.HomeNotificationAdapter
import com.example.myapplication.classpackage.GallaryFolder
import com.example.myapplication.classpackage.NotificationHome
import com.example.myapplication.classpackage.SliderImage
import com.example.myapplication.databinding.FragmentStudentGallaryBinding

class StudentGallaryFragment : Fragment() {

    private var _binding : FragmentStudentGallaryBinding?= null
    private val binding get() = _binding!!

    private lateinit var sliderImgList: ArrayList<SliderImage>
    private lateinit var sliderImageRun : Runnable
    private lateinit var sliderImageHandler: Handler
    private lateinit var sliderImageAdapter: GallarySliderAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentStudentGallaryBinding.inflate(layoutInflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sliderImageItems()
        sliderImageList()

        binding.gallaryFolderRv.layoutManager = GridLayoutManager(context,2)
        val gallaryFolderAdapter = GallaryFolderAdapter(requireContext(),gallary_folder_list())
        binding.gallaryFolderRv.adapter = gallaryFolderAdapter


    }

    fun sliderImageItems(){
        sliderImgList = ArrayList()

        sliderImageAdapter = GallarySliderAdapter(binding.gallaryViewPager, sliderImgList)
        binding.apply {
            gallaryViewPager.adapter =  sliderImageAdapter
//            gallaryViewPager.clipChildren = true
//            gallaryViewPager.clipToPadding = true
//            gallaryViewPager.offscreenPageLimit = 1
//            gallaryViewPager.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER
//            val comPosPageTarn = CompositePageTransformer()
//            comPosPageTarn.addTransformer(MarginPageTransformer(10))
//            comPosPageTarn.addTransformer { page, position ->
//                val r : Float = 1 - Math.abs(position)
//                page.scaleY = 0.85f + r * 0.15f
//            }
//            binding.gallaryViewPager.setPageTransformer(comPosPageTarn)
            sliderImageHandler = Handler()
            sliderImageRun = Runnable {
                binding.gallaryViewPager.currentItem = binding.gallaryViewPager.currentItem + 1

            }
            binding.gallaryViewPager.registerOnPageChangeCallback(
                object : ViewPager2.OnPageChangeCallback(){
                    override fun onPageSelected(position: Int) {
                        super.onPageSelected(position)
                        sliderImageHandler.removeCallbacks(sliderImageRun)
                        sliderImageHandler.postDelayed(sliderImageRun,2000)
                    }
                }
            )
        }
    }

    fun sliderImageList(){
        sliderImgList.add(SliderImage(R.drawable.gallaryimageslider1))
        sliderImgList.add(SliderImage(R.drawable.gallarysliderimage2))
        sliderImgList.add(SliderImage(R.drawable.gallarysliderimage3))
        sliderImgList.add(SliderImage(R.drawable.gallaryimageslider4))
    }

    fun gallary_folder_list() : ArrayList<GallaryFolder>{
        val list = arrayListOf<GallaryFolder>(

            GallaryFolder(
                2078,"Notice",
                arrayListOf(
                    "https://scontent.fktm3-1.fna.fbcdn.net/v/t39.30808-6/275015826_5572903782725895_172704538300408544_n.jpg?_nc_cat=105&ccb=1-5&_nc_sid=730e14&_nc_ohc=esR-0_tCpNoAX-gxS0K&_nc_ht=scontent.fktm3-1.fna&oh=00_AT8M58_aa6nyXqqqEPmCwLuPHG_Zm_XNuI_SVHnwMD0CdA&oe=622B77F7",
                    "https://scontent.fktm3-1.fna.fbcdn.net/v/t39.30808-6/274983204_5566974723318801_7519264064834825256_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=730e14&_nc_ohc=nRQ4MfFS-T4AX_IFaOS&_nc_oc=AQmIplHU1XnSH7BLpNV91AnR99CPyuBx6SpTEq8RiuUfCJorSWUvNV9Z4y07Wch_F4cTPvVozzfgEgD8ySnkhqTH&_nc_ht=scontent.fktm3-1.fna&oh=00_AT_Qw56afysR1Nhv9QwJvblB7DzYd5Po4y2_13CSR9xAXQ&oe=622B8B28",
                    "https://scontent.fktm3-1.fna.fbcdn.net/v/t39.30808-6/274226665_5535513686464905_4213372552516301961_n.jpg?_nc_cat=111&ccb=1-5&_nc_sid=730e14&_nc_ohc=BiG2ZiBQIg8AX9XnRKz&_nc_ht=scontent.fktm3-1.fna&oh=00_AT9dvoQNiV0vSK9ztFMVzzydIGYUWybOYd05lAGJaqMWlg&oe=622AE37A",
                    "https://scontent.fktm3-1.fna.fbcdn.net/v/t39.30808-6/272938065_5493960220620252_5739631989642483101_n.jpg?_nc_cat=100&ccb=1-5&_nc_sid=730e14&_nc_ohc=tPytS3J7-h0AX87oW6L&_nc_ht=scontent.fktm3-1.fna&oh=00_AT81qQ1CAahGVgYaaPIkrWltBC6ZEghtfC9rT2c1KTTaGg&oe=622A96B0",
                    "https://scontent.fktm3-1.fna.fbcdn.net/v/t39.30808-6/273286157_5483703024979305_7004099184440537703_n.jpg?_nc_cat=101&ccb=1-5&_nc_sid=730e14&_nc_ohc=70YzaNE91f8AX-lCVGI&_nc_ht=scontent.fktm3-1.fna&oh=00_AT-NSPeUAJtaJcAKLNSrA4sI-yrJrLZ6j58ZUgM--iyYyA&oe=622A50FC",

                )
            ),

            GallaryFolder(2075,"International Visits",
            arrayListOf(
                "https://lrischool.edu.np/public/uploads/galleryimage/1597912186INTERNATIONAL_VISITS.jpeg.jpeg",
                "https://lrischool.edu.np/public/uploads/galleryimage/1597912210INTERNATIONAL_VISITS1.jpeg.jpeg",
                "https://lrischool.edu.np/public/uploads/galleryimage/1597912225INTERNATIONAL_VISITS2.jpeg.jpeg",
                "https://lrischool.edu.np/public/uploads/galleryimage/1597912244IV.jpeg.jpeg",
                "https://lrischool.edu.np/public/uploads/galleryimage/1597912261IV2.jpeg.jpeg"
            )
            ),

            GallaryFolder(2076, "Rangoli Competition",
            arrayListOf(
                "https://lrischool.edu.np/public/uploads/galleryimage/16359421005.jpg.jpg",
                "https://lrischool.edu.np/public/uploads/galleryimage/16359421156.jpg.jpg",
                "https://lrischool.edu.np/public/uploads/galleryimage/16359421156.jpg.jpg",
                "https://lrischool.edu.np/public/uploads/galleryimage/16359421518.jpg.jpg",
                "https://lrischool.edu.np/public/uploads/galleryimage/16359421771.jpg.jpg"
            )),

            GallaryFolder(2078,"Foundation Day",
                arrayListOf(
                    "https://lrischool.edu.np/public/uploads/galleryimage/1645772033IMG-20220225-WA0021.jpg.jpg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/1645770581IMG-20220225-WA0003.jpg.jpg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/1645770873IMG-20220225-WA0011.jpg.jpg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/1645771967IMG-20220225-WA0018.jpg.jpg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/1645771091IMG-20220225-WA0015.jpg.jpg"
                )),

            GallaryFolder( 2078, "5th LRI MUN",
                arrayListOf(
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399686481639967728159.jpeg.jpeg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399688411639967728273.jpeg.jpeg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399686931639967728187.jpeg.jpeg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399683311639967727979.jpeg.jpeg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399683311639967727979.jpeg.jpeg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399683871639967728007.jpeg.jpeg",
                    "https://lrischool.edu.np/public/uploads/galleryimage/16399687301639967728215.jpeg.jpeg"
                )
            ),



        )
        return list
    }

 

}